// Sistema para monitoreo de huerto en Agricola Pantitlán
// Escrito por Pavel E Vazquez Mtz. <pavel_e@riseup.net>

// Registro de cambios
//
// 19/06/19:
// Lectura del sensor DHT11, escribe por el puerto serie a 19200 baudios:
// número de lectura;temeratura en grados centígrados;hunedad relativa
// en intervalos de 10 segundos



#include "DHT_U.h"  // Requiere la bibliotecas DHTsensor y Adafruit Unifid Sensor

#define DHTPIN 2     // pin para lectura del sensor DHT
#define DHTTYPE DHT11   // Tipo de sensor DHT 11
#define TIEMPO_DE_ESPERA 5000  //Retardo en la lectura

long count = 0;
DHT dht(DHTPIN, DHTTYPE); // instancia del sensor DHT11

void setup() {
  Serial.begin(19200);  //puerto serie a 19200 baudios
  Serial.println(F("#;t;h"));
  dht.begin(); // icializa el sensor dht
}

void loop() {
  
  delay(TIEMPO_DE_ESPERA);

  count++; // incrementa el numero de muestra

  float h = dht.readHumidity();  // lee temperatura
  float t = dht.readTemperature();  // lee huedadrelativa
  
  if (isnan(h) || isnan(t)) {   // Verifica errores en la lectura
    Serial.println(F("error!!!"));
    return;
  }

//  // Compute heat index in Celsius (isFahreheit = false)
//  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print(count);
  Serial.print(";");
  Serial.print(t);
  Serial.print(";");
  Serial.println(h);
}
