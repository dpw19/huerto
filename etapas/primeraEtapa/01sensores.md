# Sensor de temperatura y humedad DHT11

Es un sensor digital que mide temperatura y humedad relativa ambiental.
La características del sensor DHT11 son las siguientes:

* Rango de humedad: 20 a 90%
* Resolución de humedad: +/- 5%
* Rango de temperatura: 0 a 50 C
* Resolución de temperatura +/-2%
* Voltaje de operación: 3 a 5V

Los pines del sensor son los siguientes

|Pin1 | Pin2 | Pin3 | Pin 4|
|-----|------|------|------|
|Vcc  | Señal|NC    |GND   |

En el montaje en circuito impreso del sensor utilizaremos
la siguiente distribución de los pines:

|Pin1 | Pin2 | Pin3 |
|-----|------|------|
|Vcc  | Señal|GND   |


Se interconecta con arduino por medio de un pin digital configurado
como entrada y con la resistencia de PULL UP habilitada,
utilizaremos la alimentación directamente del pin de 5V
del arduino y la conexión a GND que también proporciona el Arduino.

| Ardino | Módulo DTH11 | Función  |
|--------|--------------|----------|
| 5V     | (Pin1)       |Vcc       |
| D3     | (pin2)       |Señal     |
| GND    | (Pin3)       |GND       |  

El programa para verificar el funcionamiento del sensor DHT11 muestra los
valores de cada lectura por medio del monitor serial y se basa en la
[biblioteca DHT11 de Adafruit](https://github.com/adafruit/DHT-sensor-library)
V1.3.4 misma que debe instalarse previamente.

Esta biblioteca incluye un código para verificar la funcionalidad del sensor.

# Código inicial

El código inicial escribe en el monitor serial el número de muestra,
la temperatura en grados centígrados y la humedad relativa en intervalos de 5 segundos

```
// Sistema para monitoreo de huerto en Agricola Pantitlán
// Escrito por Pavel E Vazquez Mtz. <pavel_e@riseup.net>

// Registro de cambios
//
// 19/06/19:
// Lectura del sensor DHT11, escribe por el puerto serie a 19200 baudios:
// número de lectura; temperatura en grados centígrados; humedad relativa
// en intervalos de 10 segundos



#include "DHT_U.h"  // Requiere la bibliotecas DHTsensor y Adafruit Unifid Sensor

#define DHTPIN 2     // pin para lectura del sensor DHT
#define DHTTYPE DHT11   // Tipo de sensor DHT 11
#define TIEMPO_DE_ESPERA 5000  //Retardo en la lectura

long count = 0;
DHT dht(DHTPIN, DHTTYPE); // instancia del sensor DHT11

void setup() {
  Serial.begin(19200);  //puerto serie a 19200 baudios
  Serial.println(F("#;t;h"));
  dht.begin(); // inicializa el sensor dht
}

void loop() {

  delay(TIEMPO_DE_ESPERA);

  count++; // incrementa el numero de muestra

  float h = dht.readHumidity();  // lee temperatura
  float t = dht.readTemperature();  // lee humedad relativa

  if (isnan(h) || isnan(t)) {   // Verifica errores en la lectura
    Serial.println(F("error!!!"));
    return;
  }

//  // Compute heat index in Celsius (isFahreheit = false)
//  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print(count);
  Serial.print(";");
  Serial.print(t);
  Serial.print(";");
  Serial.println(h);
}
```

Para capturar los resultados en un archivo podemos utilizar la siguiente
[secuencia](https://stackoverflow.com/questions/25662489/how-to-write-on-serial-port-in-python-that-ttyusb0-will-be-interpreted-commands)
en la línea de comandos de linux

```
$ stty -F /dev/ttyUSB0 -raw ispeed 19200 ospeed 19200
$ cat < /dev/ttyUSB0 > temperatura.csv
```

# Primeros resultados
El sistema inicial se mantuvo en funcionamiento por una noche completa y se 
obtuvo el siguiente resultado: Solo se muestra la parte inicial y final del archivo
temperatura.csv

```
6;29.00;18.00

7;29.00;18.00

8;29.00;18.00

9;#;t;h

1;29.00;18.00

2;29.00;18.00

3;29.00;18.00

4;29.00;18.00

5;29.00;18.00

6;29.00;18.00

7;29.00;18.00

8;29.00;18.00

9;29.00;18.00

10;29.00;18.00

11;29.00;18.00

...

5461;27.00;19.00

5462;27.00;19.00

5463;27.00;19.00

5464;27.00;19.00

5465;27.00;19.00

5466;27.00;19.00

5467;27.00;19.00

5468;27.00;19.00

5469;27.00;19.00

5470;27.00;19.00

5471;27.00;19.00

5472;27.00;19.00

5473;27.00;19.00

5474;27.00;19.00

5475;27.00;19.00

```

Para utilizr gnuplot hay que limpiar el archivo.

Primero quitamos los renglones en blanco, guardamos el resultado en el archivo temperaturas_1.csv

```
$ grep . temperaturas.csv > temperaturas-1.csv
```

Ahora cambiamos los ";" por espacios o tabuladores y los guardamos en temperatras-2.csv

```
$ tr ';' '\t' < temperaturas-1.csv > temperaturas-2.csv
```

Graficamos con [GNUPlot](http://www.gnuplot.info/) utilizando los siguientes comandos

```
> set title "Temperatura y humedad relativa"
> set xlabel "Número de Muestra, tomada cada 5 segundos"
> set ylabel "Temperatra [Grados centigrados],  Humedad Relativa [%]
> plot "temperaturas-2.csv" using 1:2 title 'Temperatura' with lines , \
"temperaturas-2.csv" using 1:3 title 'HumedadReativa' with lines

```
Está es la gráfica obtenida

![Plot de temperaturas-2.csv](plotTemperaturas-2.png "Plot temperaturas-2.csv")


# Por hacer

* Verificar las siguientes pruebas de comunicación serie con python:
  * [python](http://panamahitek.com/python-arduino-comunicacion-serial/)
  * [python](http://www.iearobotics.com/proyectos/cuadernos/ct11/ct11.html)
* Opcion 1: Guardar archivo directamente en memoria SD.
* Opcion 2: Enviar los datos directamente a un servidor.
* Agragar la medición de cantidad de luz
* Agregar sensor de humedad en la tierra

# bibliografía
* GNUPLOT a brief manual and tutorial
http://people.duke.edu/~hpgavin/gnuplot.html

* arduino.cc

* https://www.markdownguide.org/
