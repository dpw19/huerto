# Huertotrónico

Se desea encontrar las condiciones de humedad y temperatura cantidad de luz
que permitan maximizar la produccción de jitomates. Para ello 
se van a monitorear las variables antes mencionada a fin de conocer 
bajo qué condiciones se puede maximizar la producción de jitomates. 

# Primera etapa 

Habilitar la lectura y recolección de datos por medio de un 
microcontrolador (Arduino)  para las siguientes
variables:

* Humedad relativa del aire
* Humedad en la tierra
* Cantidad de luz

Esta tapa esta en una fase inicial, puede conocerse en [sensores](etapas/primeraEtapa/01sensores.md)

# Segunda etapa

Generar un arreglo que nos permita mapear las variables anteriores
en diferentes puntos del huerto

Esta etapa está en espera.

# Tercera etapa

* Encontrar la relación de las variables anteriores para obtener las
condiciones de temperatura, humedad y cantidad de luz que permitan
maximizar la producción de jitomates.

Esta etapa está en espera.
